function main() {
  START="<%="
  END="%>"

  if [ $# -lt 2 ]
  then
    echo 'Мало аргументов, ожидается: main "<file_template>" "<file_result>"' 1>&2
    exit 1
  fi

  if [ ! -f "$1" ]
  then
    echo "Файл шаблона '$1' не найден!" 1>&2
    exit 1
  fi

  cp "$1" "$2"
  for var in $(grep -o -E ${START}"[ ]*[1-9a-zA-Z_]*[ ]*"${END} "$1" | sed -E 's| |%20|g')
  do
    P1=`echo "$var" | sed 's|%20| |g'`
    var=$(echo "$var" | sed -E "s|${START}||g" | sed -E "s|${END}||g" | sed -E 's|%20||g')
    eval val=$`echo "${var^^}"`
    P2="${val}"
    sed -i "s|${P1}|${P2}|g" "$2"
  done
}


if [[ "$0" == "script.sh" || "$0" == *"/script.sh" ]]
then
  # if script start by command line
  SHORTOPTS="t:r:h"
  LONGOPTS="template:,result:,help"
  OPTS=$(getopt -o "$SHORTOPTS" -l "$LONGOPTS" -- "$@")
  USERHELP="Порядок использования: $0 -t | --template <имя шаблона>, -r | --result <имя результирующего файла>, -h | --help"

  if [ $# -lt 4 ]
  then
    echo "Мало аргументов" 1>&2
    echo $USERHELP
    exit 1
  fi

  eval set -- "$OPTS"
  while [ $# -gt 0 ]; do
    case $1 in
      -t|--template)
        TEMPLATE="$2"
        shift 2
      ;;
      -r|--result)
        RESULT="$2"
        shift 2
      ;;
      -h|--help)
         echo $USERHELP
         exit 0
      ;;
      --) shift; break;;
      *);;
    esac
  done

  main "$TEMPLATE" "$RESULT"
fi
