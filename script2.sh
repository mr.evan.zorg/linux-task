BASEDIR=$(dirname "$0")
. ${BASEDIR}/task2.conf

mkdir -p $SOURCEDIR 1>/dev/null 2>/dev/null
mkdir -p $TARGETDIR 1>/dev/null 2>/dev/null
touch $LOGFILENAME

function log() {
  echo "`date '+%Y-%m-%d %H:%M:%S'` $SOURCEDIR/$1 -> $TARGETDIR/$2" >> $LOGFILENAME
}

function move() {
  file=$1
  # делаем только если в файле есть UUID
  grep -iE "^[0-9A-F]{8}-[0-9A-F]{4}-[1-5][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$" $SOURCEDIR/$file 1>/dev/null 2>/dev/null
  if [ "$?" == 0 ]
  then
    obj_data=`head -1 $SOURCEDIR/$file 2>/dev/null | awk '{print $1}'` 2>/dev/null
    get_data=`date "+%Y-%m-%d"`
    new_file="${file}_${obj_data}_${get_data}"
    mv $SOURCEDIR/$file $TARGETDIR/$new_file 1>/dev/null 2>/dev/null && log $file $new_file
  fi
}

# делаем пока директория не пуста
# ПРИ ТАКОЙ СХЕМЕ ТЕСТЫ ЗАВИСАЮТ !!!
#while [[ `ls -U $SOURCEDIR | wc -l` != 0 ]]
#do
#  # берём рандомную выборку файлов, чтобы меньше конфликтовать с параллельными процессами
#  for file in `ls -U $SOURCEDIR | shuf -n 100`
#  do
#    move $file
#  done
#done

for f in $(find $SOURCEDIR -type f)
do
  if [[ ! -f "/tmp/${f##*/}" ]]
  then
    touch "/tmp/${f##*/}" && move ${f##*/} && rm "/tmp/${f##*/}" 2>/dev/null
  fi
done
