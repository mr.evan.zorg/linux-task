# linux-task
## полный текст заданий: linux_tasks.md)

## первое задание (про шаблон)
### краткое описание задания:
- я должен сделать скрипт script.sh
- у скрипта на входе шаблон (template.txt)
- на выходе у скрипта результирующий файл
- в скрипте надо провыерять параметры (аргументы)

про шаблон
- шаблон содержит паттерны, которые мой скрипт должен заменить на значения переменных
- переменные возьмутся из окружения
- если переменной нет, то подставляем ничего: ""
- нужно обеспечить возможность подключать скрипт как бибку
- тестируемый скрипт должен получать данные из task1.yml из корня директории Linux:
        ```YAML
        ---

        script: <имя скрипта>
        subroutine: <имя функции внутри скрипта для вызова>
        begin_patr: <Начала pattern'а>
        end_patr: <Конец Pattern'а>
        ```


### скрипт из первого задания
```
bash script.sh -t | --template <имя шаблона>, -r | --result <имя результирующего файла>, -h | --help
```
### конфигурационный файл первого задания
```
task1.yml
```
### тестирование запуска первого скрипта
```
./test_script.sh
```

## второе задание (про перенос файлов)
### краткое описание задания:
Есть директория, в неё сохраняются файлы которые имеют соответствующий формат:
- В первой строке находиться имя объекта и дата(в виде `1971-04-17 05:59:26`). разделитель табуляция (`\t`)
- В последний строке UUID
нужно написать скрипт который: 
- Должен брать из исходной директории полностью полученные файлы и переносить в результирующую директорию
- При этом файлы должны быть переименованы, так что бы имя несло в себе следующею информацию имя объекта, его дата, дата получения
- Вести лог перенесенных файлов
- Учесть возможность запуска несколько экземпляров скрипта одновременно
- Параметры (имена результирующей и исходной директории и имя файла лога) скрипт должен принимать из конфигурационного файла формата

### скрипт для генерации файлов для тестирования второго задания
```
./file_gen_task2.sh
```

### скрипт из второго задания
```
bash script2.sh
```

### конфигурационный файл второго задания
```
task1.yml
```

### тестирование запуска второго скрипта
```
./start_task2.sh
```

