export HOSTNAME="host.memo-spb.ru"
export HOME="/usr/home/victor"
export USER="victor"
export MY_VAR="это текст с пробелами"

echo "=========== template ============"
cat "debug.tpl"


echo "===========  result  ============"
while read LINE
do
  var_name=`echo $LINE | awk -F"${START}" '{print $2}' | awk -F"${END}" '{print $1}' | sed 's/" "//g'`
  eval var_val=$`echo ${var_name^^}`
  #echo "$LINE                ${START}${var_name}${END}"
  P1="${START}${var_name}${END}"
  P2="${var_val}"
  echo `echo $LINE | sed "s:$P1:$P2:g"`
done < "debug.tml"

exit 0
