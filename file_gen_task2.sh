#!/bin/bash

WD="./input"
mkdir -p $WD 1>/dev/null 2>/dev/null

for i in {1..20000}
do
  file_date=`date '+%Y-%m-%d	%H:%M:%S'`
  uuid=$(cat /proc/sys/kernel/random/uuid)
  file_name=`echo -n $uuid | openssl sha1 | awk '{print $2}'`
  cat<<EOF > $WD/$file_name
$file_date
1
2
3
4
5
6
7
8
9
10
$uuid
EOF
#  sleep 0.01
done
