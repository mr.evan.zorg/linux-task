#!/bin/bash

#while [[ `ls -U input/ | wc -l` != 0 ]]
#do
#  gen=`ps ax | grep file_gen_task2.sh | wc -l`
#  copy=`ps ax | grep script2.sh | wc -l`
#  input=`ls -U input/ | wc -l`
#  output=`ls -U output/ | wc -l`
#  clear
#  echo -e "gen: $gen\ncopy: $copy\ninput: $input\noutput: $output"
#  sleep 1
#done


mkdir -p input/ 2>/dev/null
mkdir -p output/ 2>/dev/null

watch -n0.1 'figlet -f digital "input"; ls -U input/ | wc -l; figlet -f digital "output"; ls -U output/ | wc -l; date "+%s" | md5sum | head -c1; tail ./task2.log; ls -lhF /tmp'

